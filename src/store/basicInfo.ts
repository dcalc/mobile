import { defineStore } from "pinia"
import { computed, ref } from "vue"
import { asyncComputed, until } from "@vueuse/core"
import { useCharacterStore } from "./character"
import api from "@/api"

export const useBasicInfoStore = defineStore("basicInfo", () => {
  const info_token = ref("")

  const eqpos = [
    { id: 1, type: "left", label: "上衣", top: 10, left: 47 },
    { id: 2, type: "left", label: "下装", top: 57, left: 0 },
    { id: 3, type: "left", label: "头肩", top: 10, left: 0 },
    { id: 4, type: "left", label: "腰带", top: 57, left: 47 },
    { id: 5, type: "left", label: "鞋", top: 104, left: 0 },
    { id: 6, type: "right", label: "手镯", top: 57, left: 47 },
    { id: 7, type: "right", label: "项链", top: 57, left: 0 },
    { id: 8, type: "right", label: "戒指", top: 104, left: 0 },
    { id: 9, type: "right", label: "辅助装备", top: 104, left: 47 },
    { id: 10, type: "right", label: "魔法石", top: 151, left: 0 },
    { id: 11, type: "right", label: "耳环", top: 151, left: 47 },
    { id: 12, type: "right", label: "武器", top: 10, left: 47 },
    { id: 13, type: "right", label: "称号", top: 10, left: 0 },
    // { id: 14, type: 'right', label: '副武器', top: 5, left: 87 },
    // { id: 15, type: 'left', label: '辟邪玉', top: 151, left: 47 },
    { id: 16, type: "left", label: "宠物", top: 104, left: 47 }
  ]

  const equipment_info = asyncComputed(
    async () => {
      // eslint-disable-next-line no-unused-expressions
      info_token.value
      return await api.equips()
    },
    null,
    { lazy: true }
  )

  const equipment_list = computed(() => {
    const info = equipment_info.value
    return info ? [...(info?.lv110 ?? []), ...(info?.myth ?? []), ...(info?.weapon ?? []), ...(info?.wisdom ?? []), ...(info?.title ?? []), ...(info?.pet ?? [])].filter(a => (a.id as number) < 100000) : []
  })

  const equiment_ids = computed(() => equipment_list.value.map(item => item.id))

  const trigger_list = asyncComputed(
    async () => {
      // eslint-disable-next-line no-unused-expressions
      info_token.value
      return await api.triggers()
    },
    null,
    { lazy: true }
  )

  const entry_list = asyncComputed(
    async () => {
      // eslint-disable-next-line no-unused-expressions
      info_token.value
      return await api.entries()
    },
    null,
    { lazy: true }
  )

  const details = computed(() => {
    const { platinum, jade, dress, sundries, emblem, enchanting } = useCharacterStore()
    return { jade, sundries, dress, emblem, enchanting, platinum }
  })

  const monster_list = asyncComputed(
    async () => {
      // eslint-disable-next-line no-unused-expressions
      info_token.value
      const data = await api.monsters()
      return Object.keys(data).map(key => {
        return { id: Number.parseInt(key), name: data[key.toString()].name }
      })
    },
    null,
    { lazy: true }
  )

  const scenes_list = asyncComputed(
    async () => {
      // eslint-disable-next-line no-unused-expressions
      info_token.value
      const data = await api.scenes()
      return Object.keys(data).map(key => {
        return { id: Number.parseInt(key), name: data[key.toString()].name }
      })
    },
    null,
    { lazy: true }
  )

  const get_equipment_detail = async (equ_id: ID, sj: boolean, carry: boolean, fusion_upgrade: boolean) => (await api.equipmentDetail(equ_id, sj, carry, fusion_upgrade))?.data

  const getEquip = (id?: ID) => {
    if (id) {
      return equipment_list.value.find(e => e.id == id)
    }
  }

  const load = () => {
    info_token.value = new Date().getTime().toString()
    return until([trigger_list, entry_list, monster_list, equipment_info, scenes_list]).toMatch(r => r.every(r => r != null))
  }

  const adventure = asyncComputed(async () => {
    const res = await api.adventures().then(r => r.data)
    const list: any[] = []
    let count = 0
    res.forEach((job) => {
      const jobInfo: {
        text: string
        value: string
        children?: any[]
      } = {
        text: job.title,
        // value:job.name
        value: job.name,
        children: []
      }
      job.children.forEach((child) => {
        count++
        if (child.name != "empty" && child.name != "sponsor") {
          jobInfo.children?.push({
            text: child.title,
            // value:job.name
            value: child.options && child.options.length > 0 ? count : child.name,
            children: child.options?.map(a => {
              return {
                text: `${child.title}(${a.title})`,
                value: a.class
              }
            }) ?? []
          })
        }
      })
      list.push(jobInfo)
    })
    return list
  })

  // const allroles = computed(() => {
  //   const res = []
  //   adventure.value.forEach(adventure => {
      
  //   })
  //   return res
  // })

  const detailList = [
    [
      {
        id: 1,
        type: "left",
        label: "上衣",
        top: 10,
        left: 47,
        value: "上衣",
        detail: ["growth", "growth_sj", "growth_cs", "enchanting", "socket_left", "socket_right", "cursed_type", "cursed_number"]
      },
      {
        id: 2,
        type: "left",
        label: "下装",
        top: 57,
        left: 0,
        value: "下装",
        detail: ["growth", "growth_sj", "growth_cs", "enchanting", "socket_left", "socket_right", "cursed_type", "cursed_number"]
      },
      {
        id: 3,
        type: "left",
        label: "头肩",
        top: 10,
        left: 0,
        value: "头肩",
        detail: ["growth", "growth_sj", "growth_cs", "enchanting", "socket_left", "socket_right", "cursed_type", "cursed_number"]
      },
      {
        id: 4,
        type: "left",
        label: "腰带",
        top: 57,
        left: 47,
        value: "腰带",
        detail: ["growth", "growth_sj", "growth_cs", "enchanting", "socket_left", "socket_right", "cursed_type", "cursed_number"]
      },
      {
        id: 5,
        type: "left",
        label: "鞋",
        top: 104,
        left: 0,
        value: "鞋",
        detail: ["growth", "growth_sj", "growth_cs", "enchanting", "socket_left", "socket_right", "cursed_type", "cursed_number"]
      },
      {
        id: 6,
        type: "right",
        label: "手镯",
        top: 57,
        left: 47,
        value: "手镯",
        detail: ["growth", "growth_sj", "growth_cs", "enchanting", "socket_left", "socket_right", "cursed_type", "cursed_number"]
      },
      {
        id: 7,
        type: "right",
        label: "项链",
        top: 57,
        left: 0,
        value: "项链",
        detail: ["growth", "growth_sj", "growth_cs", "enchanting", "socket_left", "socket_right", "cursed_type", "cursed_number"]
      },
      {
        id: 8,
        type: "right",
        label: "戒指",
        top: 104,
        left: 0,
        value: "戒指",
        detail: ["growth", "growth_sj", "growth_cs", "enchanting", "socket_left", "socket_right", "cursed_type", "cursed_number"]
      },
      {
        id: 9,
        type: "right",
        label: "辅助装备",
        top: 104,
        left: 47,
        value: "辅助装备",
        detail: ["growth", "growth_sj", "growth_cs", "enchanting", "socket_left", "cursed_type", "cursed_number"]
      },
      {
        id: 10,
        type: "right",
        label: "魔法石",
        top: 151,
        left: 0,
        value: "魔法石",
        detail: ["growth", "growth_sj", "growth_cs", "enchanting", "socket_left", "cursed_type", "cursed_number"]
      },
      {
        id: 11,
        type: "right",
        label: "耳环",
        top: 151,
        left: 47,
        value: "耳环",
        detail: ["growth", "growth_sj", "growth_cs", "enchanting", "cursed_type", "cursed_number"]
      },
      {
        id: 12,
        type: "right",
        label: "武器",
        top: 10,
        left: 47,
        value: "武器",
        detail: ["growth", "growth_sj", "growth_cs", "enchanting", "cursed_type", "cursed_number", "dz_number"]
      },
      {
        id: 13,
        type: "right",
        label: "称号",
        top: 10,
        left: 0,
        value: "称号",
        detail: ["enchanting"]
      }
    ],
    [
      {
        id: 101,
        type: "right",
        label: "武器装扮",
        top: 10,
        left: 141,
        value: "武器装扮",
        detail: ["enchanting", "socket_left", "socket_right"]
      },
      {
        id: 102,
        type: "right",
        label: "头发",
        top: 10,
        left: 96,
        value: "头发",
        detail: ["dress"]
      },
      {
        id: 103,
        type: "right",
        label: "帽子",
        top: 10,
        left: 47,
        value: "帽子",
        detail: ["dress"]
      },
      {
        id: 104,
        type: "right",
        label: "脸部",
        top: 10,
        left: 0,
        value: "脸部",
        detail: ["dress"]
      },
      {
        id: 105,
        type: "right",
        label: "光环",
        top: 57,
        left: 141,
        value: "光环",
        detail: ["enchanting", "socket_left", "socket_right"]
      },
      {
        id: 106,
        type: "right",
        label: "胸部",
        top: 57,
        left: 96,
        value: "胸部",
        detail: ["dress"]
      },
      {
        id: 107,
        type: "right",
        label: "上衣",
        top: 57,
        left: 47,
        value: "上衣",
        detail: ["dress"]
      },
      {
        id: 108,
        type: "right",
        label: "皮肤",
        top: 57,
        left: 0,
        value: "皮肤",
        detail: ["enchanting", "socket_left", "socket_right"]
      },
      {
        id: 109,
        type: "right",
        label: "腰带",
        top: 104,
        left: 96,
        value: "腰带",
        detail: ["dress"]
      },
      {
        id: 110,
        type: "right",
        label: "下装",
        top: 104,
        left: 47,
        value: "下装",
        detail: ["dress"]
      },
      {
        id: 111,
        type: "right",
        label: "鞋",
        top: 104,
        left: 0,
        value: "鞋",
        detail: ["dress"]
      },
      {
        id: 14,
        type: "left",
        label: "宠物",
        top: 10,
        left: 0,
        value: "宠物",
        detail: ["enchanting", "pet"]
      },
      {
        id: 113,
        type: "left",
        label: "纹章",
        top: 104,
        left: 0,
        value: "快捷装备",
        detail: ["enchanting"]
      },
      // {
      //   id: 117,
      //   type: "left",
      //   label: "护石",
      //   top: 57,
      //   left: 0,
      //   value: "护石",
      //   detail: []
      // },
      {
        id: 114,
        type: "right",
        label: "勋章",
        top: 151,
        left: 96,
        value: "勋章",
        detail: ["medal"]
      },
      {
        id: 116,
        type: "right",
        label: "辟邪玉",
        top: 151,
        left: 47,
        value: "辟邪玉",
        detail: ["jade"]
      },
      {
        id: 121,
        type: "right",
        label: "其他",
        top: 151,
        left: 0,
        value: "其他",
        detail: ["others"]
      }
    ]
  ];

  const currentRole = ref("weapon_master")

  return {
    equipment_info,
    equipment_list,
    equiment_ids,
    trigger_list,
    entry_list,
    details,
    monster_list,
    scenes_list,
    get_equipment_detail,
    getEquip,
    load,
    eqpos,
    adventure,
    detailList,
    currentRole
  }
})
