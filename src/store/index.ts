import { createPinia } from "pinia"

export * from "./character"
export * from "./basicInfo"
export * from "./config"

export default createPinia()
