// const BASE_URL = process.env.NODE_ENV === "development" ? "/static/images" : "/static/images"
const BASE_URL = "https://image.dnftools.com"

export default function useImage(space = "") {
  return (url: string) => {
    if (url.startsWith("http")) {
      return url
    } else if (url.startsWith("@")) {
      url = url.slice(1)
    } else { url = `/${space}/${url}` }

    url = url.replace(/\/\//g, "/")
    url = `${BASE_URL}${url}`
    return url
  }
}
